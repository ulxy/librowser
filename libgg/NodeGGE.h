#pragma once
#include "_ccginc.h"
#include <array>

class NodeGGE : public Node
{
protected:
	virtual bool init();
	virtual void updateColor();
	virtual void renderColor(gge::guint32 color){}
	virtual void draw(Renderer* renderer, const Mat4& transform, uint32_t flags);
	virtual void render(uint32_t flags, const std::array<Vec2, 4>& vecRenders, const Vec2& vecAp);
};

