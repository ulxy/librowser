#pragma once
#include "_coco.h"
#include "_gge42_dev18.h"
#include "../libcc/_cpp.h"
#include "../libcc/_window.h"


typedef gge::ggeTexture Texture2D;


namespace ccg
{
	inline float convertToGGE(float y){ return Director::getInstance()->getWinSize().height - y; }

	inline Vec2 convertToGGE(const Vec2& v){ return Vec2(v.x, convertToGGE(v.y)); }

	inline gge::guint32 color2gge32(const Color4B &c){ return gge::Color_ARGB(c.a, c.r, c.g, c.b); }
}

using namespace std;