/* Copyright (c) 2012 Scott Lembcke and Howling Moon Software
 * Copyright (c) 2012 cocos2d-x.org
 * Copyright (c) 2013-2017 Chukong Technologies Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Code copied & pasted from SpacePatrol game https://github.com/slembcke/SpacePatrol
 *
 * Renamed and added some changes for cocos2d
 *
 */

#ifndef __CCDRAWNODES_CCDRAW_NODE_H__
#define __CCDRAWNODES_CCDRAW_NODE_H__

#include "_ccginc.h"
NS_CC_BEGIN
/**
 * @addtogroup _2d
 * @{
 */

/** @class DrawNode
 * @brief Node that draws dots, segments and polygons.
 * Faster than the "drawing primitives" since they draws everything in one single batch.
 * @since v2.1
 */
class CC_DLL DrawNode : public Node
{
public:
    /** creates and initialize a DrawNode node.
     *
     * @return Return an autorelease object.
     */
//    static DrawNode* create();
	CREATE_FUNC(DrawNode);

    /** Draw an line from origin to destination with color. 
     * 
     * @param origin The line origin.
     * @param destination The line destination.
     * @param color The line color.
     * @js NA
     */
    void drawLine(const Vec2 &origin, const Vec2 &destination, const Color4F &color);
    
    /** Draws a rectangle given the origin and destination point measured in points.
     * The origin and the destination can not have the same x and y coordinate.
     *
     * @param origin The rectangle origin.
     * @param destination The rectangle destination.
     * @param color The rectangle color.
     */
    void drawRect(const Vec2 &origin, const Vec2 &destination, const Color4F &color);


	void drawPoly(const Vec2 *poli, unsigned int numberOfPoints, bool closePolygon, const Color4F &color);
    
 
	void clear();
CC_CONSTRUCTOR_ACCESS:
	DrawNode(){}
	virtual ~DrawNode(){}
//    virtual bool init() override;
private:
    CC_DISALLOW_COPY_AND_ASSIGN(DrawNode);

protected:
	enum class eDraw{ Line, Rect, Poly };
	struct sLine{ Vec2 origin, destination; Vec2 vecOrigin, vecDestin; gge::guint32 color; };
	struct sRect{ Vec2 origin, destination; Vec2 vecOrigin, vecDestin, vecOxdy, vecDxoy; gge::guint32 color; };
	struct sPoly{ const Vec2* poli; unsigned int numberOfPoints; vector<Vec2> polies; bool closePolygon; gge::guint32 color; };
	struct sDraw
	{
		eDraw e;
		sLine line;
		sPoly poly;
	};
	vector<sLine> _lines;
	vector<sRect> _rects;
	vector<sPoly> _polies;
	vector<eDraw> _enums;

// 	void updateDraw(sLine& line, const AffineTransform& affineTransform);
// 	void updateDraw(sRect& rect, const AffineTransform& affineTransform);
// 	void updateDraw(sPoly& poly, const AffineTransform& affineTransform);

	virtual void draw(Renderer* renderer, const Mat4& transform, uint32_t flags);
};
/** @} */
NS_CC_END

#endif // __CCDRAWNODES_CCDRAW_NODE_H__
