#include "NodeGGE.h"

// static Vec2 affineTransform2render(const AffineTransform& affineTransform, const Size& size, Vec2 vAp)
// {
// 	vAp.x *= size.width;
// 	vAp.y *= size.height;
// 	vAp = __CCPointApplyAffineTransform(vAp, affineTransform);
// 	return ccg::convertToGGE(vAp);
// }
//
// inline Color4F gge2color(const gge::ggeColor &c){ return Color4F(c.r, c.g, c.g, c.a); }
// inline gge::ggeColor color2gge(const Color4F &c){ return gge::ggeColor(c.r, c.g, c.b, c.a); }
// inline Color3B gge2color(gge::guint32 c){ return Color3B(gge::Color_GetR(c), gge::Color_GetG(c), gge::Color_GetB(c)); }
// inline gge::guint32 color2gge(const Color4B &c){ return gge::Color_ARGB(c.a, c.r, c.g, c.b); }
// inline gge::guint32 color2gge(const Color3B &c){ return gge::Color_ARGB(0xff, c.r, c.g, c.b);

bool NodeGGE::init()
{
	if (!Node::init())
	{
		return false;
	}
	_cascadeColorEnabled = true;
	_cascadeOpacityEnabled = true;
	return true;
}


void NodeGGE::updateColor()
{
	Color4B c4(getDisplayedColor());
	c4.a = getDisplayedOpacity();
	renderColor(ccg::color2gge32(c4));
}

void NodeGGE::draw(Renderer* renderer, const Mat4& transform, uint32_t flags)
{
	std::array<Vec2, 4> vecRenders;
	Vec2 vecAp;
	if (flags)
	{
		vecAp = this->convertToWorldSpace(Vec2::ZERO);
		Vec2 size = this->convertToWorldSpace(_contentSize) - vecAp;
		vecRenders[0].x = vecRenders[2].x = vecAp.x;
		vecRenders[1].x = vecRenders[3].x = vecAp.x + size.x;
		vecRenders[0].y = vecRenders[1].y = ccg::convertToGGE(vecAp.y + size.y);
		vecRenders[2].y = vecRenders[3].y = ccg::convertToGGE(vecAp.y);
		vecAp = this->convertToWorldSpace(getAnchorPointInPoints());
		vecAp.y = ccg::convertToGGE(vecAp.y);

// 		const auto& affineTransform = getNodeToWorldAffineTransform();
// 		vecRenders[0] = affineTransform2render(affineTransform, _contentSize, Vec2::ANCHOR_TOP_LEFT);
// 		vecRenders[1] = affineTransform2render(affineTransform, _contentSize, Vec2::ANCHOR_TOP_RIGHT);
// 		vecRenders[2] = affineTransform2render(affineTransform, _contentSize, Vec2::ANCHOR_BOTTOM_LEFT);
// 		vecRenders[3] = affineTransform2render(affineTransform, _contentSize, Vec2::ANCHOR_BOTTOM_RIGHT);
// 		vecAp = affineTransform2render(affineTransform, _contentSize, getAnchorPoint());
	}
	render(flags, vecRenders, vecAp);
}

void NodeGGE::render(uint32_t flags, const std::array<Vec2, 4>& vecRenders, const Vec2& vecAp)
{

}

