#pragma once

#include "AppDelegate.h"
#include "DrawNode.h"
#include "GLViewImpl.h"
#include "LabelFont.h"
#include "Layer.h"
#include "NodeGGE.h"
#include "Sprite.h"

typedef Layer LayerColor;