﻿#include "LabelFont.h"

bool LabelFont::init(const std::string& fontname, int fontSize)
{
	if (!NodeGGE::init())
	{
		return false;
	}
	_fnt = gge::Font_Create(fontname.c_str(), fontSize, gge::FONT_CREATE_MODE::FONT_MODE_MONO);
	if (_fnt == nullptr)
	{
		MessageBoxA(nullptr, fontname.c_str(), __FUNCTION__, MB_OK);
		return false;
	}
	_fnt->SetAlign(gge::FONT_ALIGN::TEXT_CENTER);
	return true;
}


void LabelFont::setString(const std::string& text)
{
	_text = text;
	static gge::ggeFont::StringInfo s_info;
	_fnt->GetStringInfo(_text.c_str(), s_info);
	NodeGGE::setContentSize(Size(s_info.Width, s_info.Height));
}

void LabelFont::render(uint32_t flags, const std::array<Vec2, 4>& vecRenders, const Vec2& vecAp)
{
	if (flags)
	{

// 		vecRenders[0] = affineTransform2render(affineTransform, _contentSize, Vec2::ANCHOR_TOP_LEFT);
// 		vecRenders[1] = affineTransform2render(affineTransform, _contentSize, Vec2::ANCHOR_TOP_RIGHT);
// 		vecRenders[2] = affineTransform2render(affineTransform, _contentSize, Vec2::ANCHOR_BOTTOM_LEFT);
// 		vecRenders[3] = affineTransform2render(affineTransform, _contentSize, Vec2::ANCHOR_BOTTOM_RIGHT);

		_vecRender = (vecRenders[0] + vecRenders[1]) / 2;
	}
	_fnt->Render(_vecRender.x, _vecRender.y, _text.c_str());
}


