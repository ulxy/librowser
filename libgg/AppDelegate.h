#ifndef  _APP_DELEGATE_H_
#define  _APP_DELEGATE_H_
#include "_ccginc.h"



class  AppGGE : public gge::ggeApplication
{
public:
	/// 游戏启动前该函数被调用，如果返回false游戏退出
	virtual bool OnConfig();

	/// 游戏启动后该函数被调用，如果返回false游戏退出
	virtual bool OnInitiate();

	/// 刷新
	virtual void OnUpdate(float dt);

	/// 渲染
	virtual void OnRender();

	/// 产生Dump时调用，dir为Dump保存目录，可以此处写入一些调试信息到log里，弹出提示窗口等
	virtual void OnMiniDump(const char *dir);


	/// 游戏退出时前该函数被调用
	virtual void OnRelease();

protected:
	Director *_director = nullptr;
public:
	function<void(Director*)> onApplicationDidFinishLaunching = nullptr;
	function<void(Director*)> onRunWithScene = nullptr;
};





/**
@brief    The cocos2d Application.

The reason for implement as private inheritance is to hide some interface call by Director.
*/
NS_CC_BEGIN
class  AppDelegate : public Application
{
public:
    /**
    @brief    Implement Director and Scene init code here.
    @return true    Initialize success, app continue.
    @return false   Initialize failed, app terminate.
    */
	virtual bool applicationDidFinishLaunching(){ return true; }

    /**
    @brief  The function be called when the application enter background
    @param  the pointer of the application
    */
	virtual void applicationDidEnterBackground(){ Director::getInstance()->stopAnimation(); }

    /**
    @brief  The function be called when the application enter foreground
    @param  the pointer of the application
    */
	virtual void applicationWillEnterForeground(){ Director::getInstance()->startAnimation(); }


	virtual void setAnimationInterval(float interval) override{ gge::System_SetState(gge::GGE_FPS, 1.0f / interval); }

public:
};
NS_CC_END
#endif // _APP_DELEGATE_H_

