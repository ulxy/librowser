﻿#pragma once
#include "Label1.h"
#include "LayerColor.h"
#include "DrawNode.h"

// 
// class Label3 : public Label2
// {
// public:
// 	static Label3* create(const string &text){ CREATE(Label3, text); }
// protected:
// 	virtual bool init(const string &text);
// };
// 
// 
// class WinPad;
// class Button : public Sprite2
// {
// public:
// 	static Button* create(float w, float h){ return create(Size(w, h)); }
// 	static Button* create(const Size &size){ CREATE(Button, 0, size); }
// 	static Button* create(ulong uid){ CREATE(Button, uid, Size::ZERO); }
// 	static Button* create2(const string &text)
// 	{
// 		Button *btn = create(cfg::uBtn2);
// 		btn->setString(text);
// 		return btn;
// 	}
// 	static Button* create4(const string &text)
// 	{
// 		Button *btn = create(cfg::uBtn4);
// 		btn->setString(text);
// 		return btn;
// 	}
// 
// 	virtual void setString(const string &text){ if (_label) _label->setString(text); }
// protected:
// 	virtual bool init(ulong uid, const Size& size);
// 
// 	Label *_label = nullptr;
// 	Vec2 _vecLabal;
// public:
// 	function<void(Button*)> onClick = nullptr;
// private:
// 	friend class WinPad;
// 	WinPad *_notify = nullptr;
// };



class Pad;
class Button1 : public Node
{
public:
	virtual ~Button1();
	static Button1* create(const string& text = "", const Size& size = Size(108, 32))
	{
		CREATE(Button1, text, size);
	}

	virtual void setString(const string &text);
	virtual void setEnabled(bool isEnabled);
	virtual bool isEnabled()const{ return _isEnabled; }
	virtual void setSpriteVisible(bool visible){ _isSpriteVisible = visible; }
protected:
//	virtual bool init(ulong uid, const string& filename, const string& text);
//	virtual bool init(ulong uid, const string& filename, int edge, const Size& contentSize, const string& text);
	virtual bool init(const string& text, const Size& size);

	virtual void renderColor(gge::guint32 color){ _sprite->SetColor(color); }
	virtual void render(uint32_t flags, const array<Vec2, 4>& vecRenders, const Vec2& vecAp);
	Pad* _pad = nullptr;
	// 1down 2over 
	virtual bool setFrame(int frame);

	gge::ggeSprite *_sprite = nullptr;
	Label1 *_label = nullptr;
	DrawNode *_drawNode = nullptr;
//	string _text;
	bool _isEnabled = true;
	bool _isSpriteVisible = true;
	int _frame = 0;
// 	function<void(Button1*)> _onNormal = nullptr;
// 	function<void(Button1*)> _onOver = nullptr;
// 	function<void(Button1*)> _onDown = nullptr;
// 	function<void(Button1*)> _onDisabled = nullptr;
//  private:
//	Color3B _colorLabel = Color3B::WHITE;
public:
	// use removeSelf
	function<void(Button1*)> onClick = nullptr;
	function<void(Button1*, float)> onKeep = nullptr;
	friend class Pad;
	friend class PadManager;
};


// class Button2 : public Button1, public Sprite2, public Skin
// {
// public:
// 	static Button2* create(ulong uid, const string& text = ""){ CREATE(Button2, uid, "", text); }
// protected:
// 	virtual bool init(ulong uid, const string& filename, const string& text);
// 	virtual bool setFrame(int frame){ return Sprite2::setFrame(frame) && Button1::setFrame(frame); }
// };
// 
// 
// class Button9 : public Button1, public Sprite9, public Skin
// {
// public:
// 	static Button9* create(ulong uid, const Size& size, const string& text = ""){ CREATE(Button9, uid, "", size, text); }
// protected:
// 	virtual bool init(ulong uid, const string& filename, const Size& size, const string& text);
// 	virtual bool setFrame(int frame){ return Sprite9::setFrame(frame) && Button1::setFrame(frame); }
// };

// 
// class WinPad : public Sprite2
// {
// public:
// 	static WinPad* create(ulong uid){ CREATE(WinPad, uid); }
// 	virtual void addChildPosition(Node *node, float x, float y);
// 	virtual void addChildPosition(Node *node, const Vec2 &vec){ addChildPosition(node, vec.x, vec.y); }
// 
// 	virtual void setString(const string &text){ if (_label) _label->setString(text); }
// 	virtual void setCloseBtn(bool isShow){ if (_btnClose)_btnClose->setVisible(isShow); }
// 	virtual void setClose(bool isClose){ _isClose = isClose; }
// 	virtual void reorder(){ getParent()->reorderChild(this, getLocalZOrder()); }
// 
// protected:
// 	virtual bool init(ulong uid);
// 
// 	bool _isClose = true;
// 	Label *_label = nullptr;
// 	Button *_btnClose = nullptr;
// 	function<void(WinPad*, bool)> onOpenClose = nullptr;
// 
// private:
// 	friend class Button;
// 	function<void(Button*)> onNotify = nullptr;
// };
// 

class PadManager;
class Pad : public LayerColor
{
public:
// 	static Pad* create(ulong uid, const string& filename, int edge, const Size& contentSize, bool isHadBtnClose = true, const string& text = "")
// 	{
// 		CREATE(Pad, uid, filename, edge, contentSize, isHadBtnClose, text);
// 	}
// 	static Pad* create(ulong uid, const string& filename, int edge, const Size& contentSize, const string& text)
// 	{
// 		CREATE(Pad, uid, filename, edge, contentSize, true, text);
// 	}
// 	static Pad* create(ulong uid, const string& filename, const Size& contentSize, bool isHadBtnClose = true, const string& text = "")
// 	{
// 		CREATE(Pad, uid, filename, cfg::edgeSprite9, contentSize, isHadBtnClose, text);
// 	}
// 	static Pad* create(ulong uid, const string& filename, const Size& contentSize, const string& text)
// 	{
// 		CREATE(Pad, uid, filename, cfg::edgeSprite9, contentSize, true, text);
// 	}
// 	static Pad* create(ulong uid, int edge, const Size& contentSize, bool isHadBtnClose = true, const string& text = "")
// 	{
// 		CREATE(Pad, uid, "", edge, contentSize, isHadBtnClose, text);
// 	}
// 	static Pad* create(ulong uid, int edge, const Size& contentSize, const string& text)
// 	{
// 		CREATE(Pad, uid, "", edge, contentSize, true, text);
// 	}
// 	static Pad* create(ulong uid, const Size& contentSize, bool isHadBtnClose = true, const string& text = "")
// 	{
// 		CREATE(Pad, uid, "", cfg::edgeSprite9, contentSize, isHadBtnClose, text);
// 	}
// 	static Pad* create(ulong uid, const Size& contentSize, const string& text)
// 	{
// 		CREATE(Pad, uid, "", cfg::edgeSprite9, contentSize, true, text);
// 	}
// 
// 
// 	static Pad* create(ulong uid, const string& filename, bool isHadBtnClose = true, const string& text = "")
// 	{
// 		CREATE(Pad, uid, filename, isHadBtnClose, text);
// 	}
// 	static Pad* create(ulong uid, const string& filename, const string& text)
// 	{
// 		CREATE(Pad, uid, filename, true, text);
// 	}
// 	static Pad* create(ulong uid, bool isHadBtnClose = true, const string& text = "")
// 	{
// 		CREATE(Pad, uid, "", isHadBtnClose, text);
// 	}
// 	static Pad* create(ulong uid, const string& text)
// 	{
// 		CREATE(Pad, uid, "", true, text);
// 	}

	static Pad* create(const Size& size, const string& text = "", bool isHadBtnClose = true)
	{
		CREATE(Pad, size, text, isHadBtnClose);
	}

	virtual void addChildSnatch(Button1 *btn);
	virtual void removeChildSnatch(Button1 *btn);
	virtual void setDragged(bool isEnable){ _isDragged = isEnable; }
	virtual void setCloseRup(bool isEnable){ _isCloseRup = isEnable; }

//	virtual void setString(const string &text){ if (_label) _label->setString(text); }

	virtual void setVisible(bool visible);

// 	// 不能添加文字
// 	virtual void addChildPosition(Node *node, float x, float y)
// 	{
// 		addChildSnatch((Button1*)node);
// 		node->setPositionX(x);
// 		node->setPositionY(_contentSize.height - y/* - btn->getContentSize().height*/);
// 		node->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
// 	}
protected:
 	virtual bool init(const Size& size, const string& text, bool isHadBtnClose);
// 	virtual bool init(ulong uid, const string& filename, bool isHadBtnClose, const string& text);
// 	virtual bool init(ulong uid, const string& filename, int edge, const Size& contentSize, bool isHadBtnClose, const string& text);

	Label1 *_label = nullptr;
	DrawNode *_drawNode = nullptr;
	bool _isDragged = false;
	bool _isCloseRup = true;
    // Pad::converToNodeSpace(Vec2)
// 	function<void(Pad*, Vec2)> _onOver = nullptr;
// 	function<void(Pad*, Vec2)> _onLdown = nullptr;
// 	function<void(Pad*, Vec2)> _onTouching = nullptr;
// 	function<void(Pad*, Vec2)> _onLup = nullptr;
// 	function<void(Pad*, Vec2)> _onRup = nullptr;
	PadManager* _palManager = nullptr;
	vector<Button1*> _buttons;
	Button1* getButton(Vec2 location);
//	Node* _nodeCurrent = nullptr;
public:
	function<void(Pad*, bool)> onVisible = nullptr;
	friend class PadManager;
};

// class Pad2 : public Sprite2, public Pad, public Skin
// {
// public:
// 	static Pad2* create(ulong uid, const string& text = ""){ CREATE(Pad2, uid, "", true, text); }
// 	static Pad2* create(ulong uid, bool isCloseBtn, const string& text = ""){ CREATE(Pad2, uid, "", isCloseBtn, text); }
// 
// 	virtual void setVisible(bool visible){ Sprite2::setVisible(visible); Pad::setVisible(visible); }
// 
// protected:
// 	virtual bool init(ulong uid, const string& filename, bool isCloseBtn, const string& text);
// };
// 
// class Pad9 : public Sprite9, public Pad, public Skin
// {
// public:
// 	static Pad9* create(ulong uid, const Size& size, const string& text = ""){ CREATE(Pad9, uid, "", size, true, text); }
// 	static Pad9* create(ulong uid, const Size& size, bool isCloseBtn, const string& text = ""){ CREATE(Pad9, uid, "", size, isCloseBtn, text); }
// 
// 	virtual void setVisible(bool visible){ Sprite9::setVisible(visible); Pad::setVisible(visible); }
// protected:
// 	virtual bool init(ulong uid, const string& filename, const Size& size, bool isCloseBtn, const string& text);
// };


class PadManager : public Node
{
public:
	CREATE_FUNC(PadManager);
	virtual void addChild(Pad* pad);
protected:
	virtual bool init();
private:
	void resortPad();
	Pad* getPadTop(Vec2 location);
	virtual void update(float delta);

	Pad* _padTop = nullptr;
	Button1* _btn = nullptr;
//	bool _isTouching = false;
	int _z = 0;

	Vector<Node*> _sortPads;
//	Vec2 _vecDrag;
	float _delta;
	friend class Pad;
};