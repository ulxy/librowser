﻿#pragma once
#include "cfg.h"

class InputBox : public Layer, public IMEDelegate, public TextFieldDelegate
{
public:
	static InputBox* create(const std::string &textHint, int limit, int fntSize = cfg::fntSizeNormal){ CREATE(InputBox, textHint, limit, fntSize); }

	virtual void setPositionForIME(Vec2 vecBegin, Vec2 vecInput);

	virtual std::string getString(){ return _textFiel->getString(); }


protected:
	virtual bool init(const std::string &textHint, int limit, int fntSize);

	virtual void keyboardWillShow(IMEKeyboardNotificationInfo& info);
	virtual void keyboardDidHide(IMEKeyboardNotificationInfo& info);

	virtual bool onTextFieldInsertText(TextFieldTTF * sender, const char * text, size_t nLen);

	virtual bool onTouchBegan(Touch *touch, Event *unused_event){ return true; }
	virtual void onTouchEnded(Touch *touch, Event *unused_event);

	int _limit;
	TextFieldTTF *_textFiel = nullptr;
	Vec2 _vecBegin;
	Vec2 _vecInput;
	bool _isSetPositionForIME = false;
};