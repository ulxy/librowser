﻿#pragma once
#include "_utilsinc.h"

namespace cfg
{
	bool isFolderRes(std::string path);
	bool setFolderRes(const std::string& path);
	const std::string& getFolderRes();
	const std::string& getFolderMap();
	const std::string& getFolderWdf();

	const std::string& getFntName();

	WdfReader* getWdfSmap();
	WdfReader* getWdf5();
	WdfReader* getWdf171();
	WdfReader* getWdfMagic();
	WdfReader* getWdfSound();
	WdfReader* getWdfMusic();
	WdfReader* getWdfTxt();
	WdfReader* getWdfColor();


	static const Color3B colorDescTitle = Color3B::YELLOW;
	static const Color3B colorDescDesc = Color3B::WHITE;
	static const Color3B colorDescText = Color3B::YELLOW;




	// pad
	static const ulong uBuy = 3929029437;
	static const ulong uDialog = 0x73D983B7;
	static const ulong uSystem = 0x694F39D1;
	static const ulong uFriend = 3482058907;
	static const ulong uGrid9 = 0x907E024C;
	static const ulong uBattleSkillPad6 = 0x2FD95E30;
	static const ulong uBattleSkillPad10 = 0x69823EE5;
	static const ulong uBattleSkillPad14 = 0x0A8B7D87;
	static const ulong uBattleSkillPad20 = 0x32F119A5;
	static const ulong uBattleSkillPad30 = 0x4226BD41;

	// button
	static const ulong uBtnCancal = 0xF11233BB;
	static const ulong uBtnLeft = 0xB05126E8;
	static const ulong uBtnRight = 0x26066130;
	static const ulong uBtnPetUp = 0x08E2D199;
	static const ulong uBtnPetDown = 0xC013B53B;
	static const ulong uBtnJobUp = 149082521;
	static const ulong uBtnJobDown = 153189907;
	static const ulong uBtn2 = 0x35A981D3;
	static const ulong uBtn1 = 0x79D01E0E;
	static const ulong uBtn3 = 0x2BD1DEF7;
	static const ulong uBtn4 = 0x86D66B9A;
	static const ulong uBtnAdd = 0xC47885C6;
	static const ulong uBtnMinus = 1577923263;
	static const ulong uBtnZZ = 0xB15C5678;
	static const ulong uBtnPetSkillDown = 0xCB50AB1D;


	static const ulong uItemOver = 0x6F88F494;
	static const ulong uItemSelect = 0x10921CA7;
	// 就一块帧矩形(白蓝黄+?)... 0xF151309F
	static const ulong uFriendOver = 0xF151309F;
	static const ulong uFriendSelect = 0;
	static const ulong uXiulianOver = 0x436592D8;
	// 0x8F43CE91 800蓝布条子
	static const ulong uFBOver = 0xF151309F;// 0x8F43CE91;
	static const ulong uEquipBack = 0xA393A808;

	static const ulong uShadow = 3705976162;
	static const ulong uIcon32 = 326702364;
	static const ulong uIcon40 = 2574462722;
	static const ulong uIcon50 = 2535965770;

	static const ulong uHeadRole = 0x2178F38B;
	static const ulong uHeadPet = 0x203CE1F7;

	static const ulong wasLvup = 0x9B3AF4E5;
	// 	m_MagicAddHP.Load(0x77D2082D);
	// 	m_MagicAddMP.Load(0x3E912705);
	// 	m_MagicAddHPMP.Load(0x808CEB72);
	static const ulong wasTransmit = 0x7F4CBC8C;

};