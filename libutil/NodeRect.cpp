﻿#include "NodeRect.h"
#include "ScissorStack.h"

bool NodeRect::init(const Vec2& xy, const Size& wh)
{
	if (!Node::init())
	{
		return false;
	}
	_cascadeColorEnabled = true;
	_cascadeOpacityEnabled = true;

	setIgnoreAnchorPointForPosition(false);
	setAnchorPoint(Vec2::ZERO);
	setRect(xy, wh);

	return true;
}


void NodeRect::visit(Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlag)
{
#ifdef ccc_use_engine_g2d
// 	Rect rect;
// 	rect.origin = this->convertToWorldSpace(_rect.origin);
// 	rect.size = _rect.origin + _rect.size;
// 	rect.size = this->convertToWorldSpace(rect.size) - rect.origin;
// 	rect.origin.y = ccg::convertToGGE(rect.origin.y + rect.size.height);
//	ScissorStack::getInstance()->push(rect);
	Node::visit(renderer, parentTransform, parentFlag);
//	ScissorStack::getInstance()->pop();
#else 
	_beforeDrawCommand.init(this->getGlobalZOrder());
	_beforeDrawCommand.func = [=]()
	{
		Rect rect;
		rect.origin = this->convertToWorldSpace(_rect.origin);
		rect.size = _rect.origin + _rect.size;
		rect.size = this->convertToWorldSpace(rect.size) - rect.origin;
		ScissorStack::getInstance()->push(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
	};
	renderer->addCommand(&_beforeDrawCommand);

	Layer::visit(renderer, parentTransform, parentFlags);

	_afterDrawCommand.init(this->getGlobalZOrder());
	_afterDrawCommand.func = []()
	{
		ScissorStack::getInstance()->pop();
	};
	renderer->addCommand(&_afterDrawCommand);
#endif
}

