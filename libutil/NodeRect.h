﻿#pragma once
#include "_utilsinc.h"

class NodeRect : public Node
{
public:
	static NodeRect* create(const Rect& rect){ CREATE(NodeRect, rect.origin, rect.size); }
	static NodeRect* create(const Vec2& xy, const Size& wh){ CREATE(NodeRect, xy, wh); }
	static NodeRect* create(int x, int y, int w, int h){ CREATE(NodeRect, Vec2(x, y), Size(w, h)); }

	virtual void setRect(const Rect& rect){ setRect(rect.origin, rect.size); }
	virtual void setRect(const Vec2& xy, const Size& wh){ _rect.origin = xy; _rect.size = wh; }
protected:
	virtual bool init(const Vec2& xy, const Size& wh);
	virtual void visit(Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags);
#ifdef ccc_use_engine_g2d
#else
	// 不是Ref，所以必须放在Node的属性中
	cocos2d::CustomCommand _beforeDrawCommand;
	cocos2d::CustomCommand _afterDrawCommand;
#endif
	Rect _rect;
};