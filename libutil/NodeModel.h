﻿#pragma once
#include "NodeSnatch.h"
#include "Sprite2.h"
#include "Label2.h"

class NodeModel : public Node, public NodeSnatch
{
public:
	static const ulong UID_SHADOW = 3705976162;
	static NodeModel* create(Node* parentForLael, const cpp::sPointer& body, const string& name, const string& appelation = "")
	{ 
		CREATE(NodeModel, parentForLael, body, name, appelation);
	}

	// 站 走 跑
	virtual Sprite2* addChildBody(const cpp::sPointer& body);
//	virtual void removeChildBodys();
	virtual Sprite2* addChildTop(const cpp::sPointer& top);
//	virtual void removeChildTops();
	virtual Sprite2* addChildTopBlood(const cpp::sPointer& topBlood);
//	virtual void removeChildTopBloods();
	virtual Sprite2* addChildUp(const cpp::sPointer& up);
//	virtual void removeChildUps();
	virtual Sprite2* addChildDown(const cpp::sPointer& down);
//	virtual void removeChildDowns();
	virtual Sprite2* addChildFoot(const cpp::sPointer& foot);
//	virtual void removeChildFoots();

	virtual void setDirection(const eDirection& direction);

	virtual void setBodyYellow(bool isYellow);
	virtual void setBlink(bool isBlink);

	virtual void setNameColor(const Color3B& c);
	virtual void setNameColorRand(bool isActing);

protected:
	virtual bool init(Node* parentForLael, const cpp::sPointer& body, const string& name, const string& appelation);
	virtual void update(float delta);
	virtual bool isBox(const Vec2& worldSpace);
	virtual void setState(const eState& state);
	virtual void doClick(const Vec2& location, bool isActiveForListener);

	virtual Sprite2* addChildFromPointer(Node* node, const cpp::sPointer& pter);

	string _name;
	string _appellation;

	Node* _nodeTop = nullptr;
	Node* _nodeTopBlood = nullptr;
	Node* _nodeBody = nullptr;
	Node* _nodeUp = nullptr;
	Node* _nodeDown = nullptr;
	Sprite2* _spriteShadow = nullptr;
	Node* _nodeFoot = nullptr;
	Label2* _LabelAppelation = nullptr;
	Label2* _LabelName = nullptr;

	bool _isBlinking = false;
	bool _isBlinkingWhenCrossing = false;
	Action* _actionBlinkYellow = nullptr;
	Action* _actionNameColorRand = nullptr;
};

