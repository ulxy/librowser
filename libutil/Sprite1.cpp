#include "Sprite1.h"

Sprite1::~Sprite1()
{
	if (_textures)
	{
		for (int i = _decoder.info->framesCount - 1; i >= 0; --i)
		{
			if (_textures[i])
			{
				_textures[i]->Release();
			}
		}
	}
// 	ccc_delete_array(_framexs);
// 	ccc_delete_array(_frameys);
// 	ccc_delete_array(_locks);
}


bool Sprite1::init(const cpp::sPointer& pointer/*, const WasReader::sColor* colorData*/)
{
	_info.load(pointer.ptr);
	if (!_info.isValid())
	{
		return false;
	}
	_decoder.load(pointer.ptr, &_info);
// 	if (!_was.init(pointer.ptr, pointer.size))
// 	{
// 		return false;
// 	}
// 	if (!_was.load(colorData))
// 	{
// 		return false;
// 	}

//	int total = _was.dCount * getFrameCountPD();
	_textures = new gge::ggeTexture* [_decoder.info->framesCount];
	for (int i = _decoder.info->framesCount - 1; i >= 0; --i)
	{
		_textures[i] = nullptr;
	}

// 	this->addChild(_sprite = Sprite::create());
// 	_sprite->setPosition(_was.kx, _was.h - _was.ky);
// 	this->setContentSize(Size(_was.w, _was.h));
// 	this->setAnchorPoint(Vec2(_was.kx * 1.0f / _was.w, 1 - _was.ky * 1.0f / _was.h));
	initSprite();
 	setTexture(0);
	return true;
}


bool Sprite1::setDirection(const eDirection direction)
{
	if (_direction == direction)
	{
		return true;
	}
	_direction = direction;
	bool b = true;
	if ((int)_direction >= getDirectionsCount())
	{
		b = false;
		if (getDirectionsCount() == 4)
		{
			_direction = eDirection((int)direction - 4);
		}
		else
		{
			_direction = eDirection(getDirectionsCount() - 1);
		}
	}
	setTexture(_frameIndex);
	return b;
}


bool Sprite1::setFrame(int frame)
{
	if (_frameIndex == frame)
	{
		return true;
	}
	_frameIndex = frame;
	bool b = true;
	if (_frameIndex >= getFrameCountPD())
	{
		b = false;
		_frameIndex = getFrameCountPD() - 1;
	}
	setTexture(_frameIndex);
	return b;
}

bool Sprite1::setTexture(int index)
{
	if (!_textures)
	{
		return false;
	}

	const int i = (int)_direction * getFrameCountPD() + _frameIndex;
	const auto& frame = _decoder.info->frames[i];
	auto& tex = _textures[i];
	if (!tex)
	{
		tex = gge::Texture_Create(std::max(frame.w, 1), std::max(frame.h, 1), gge::TEXTURE_FORMAT::TEXFMT_A4R4G4B4);
		if (frame.w > 4 && frame.h > 4)
		{
			auto deco = _decoder.getDecode(i);
			auto p565 = _decoder.get565(i);
			if (deco->isValid())
			{
				int pitch;
				auto lock = (ushort*)tex->Lock(false, 0, 0, frame.w, frame.h, &pitch);
				int i2;
				for (int ih = frame.h - 1; ih >= 0; --ih)
				{
					for (int iw = frame.w - 1; iw >= 0; --iw)
					{
						i2 = ih * frame.w + iw;
						lock[ih * pitch / 2 + iw] = p565[i2].to4444(deco->alphas[i2]);
					}
				}
				tex->Unlock();
			}

		}
	}

	setTexture(tex, frame);

// 	int idx = _decoder.info->framesCount - 1;
// 	for (; i >= 0; --idx)
// 	{
// 		if (!_textures[idx])
// 		{
// 			break;
// 		}
// 	}
// 	if (idx < 0)
// 	{
// 		_was.clearPointer();
// 	}
	return true;
}

bool Sprite1::setTexture(Texture2D* texture, const sSpFrame& frame)
{
//	NodeGGE::setContentSize(Size(_textures[i]->GetWidth(), _textures[i]->GetHeight()));
// 	_sprite->setTexture(texture);
// 	_sprite->setAnchorPoint(Vec2(frame.x / _sprite->getContentSize().width, 1 - frame.y / _sprite->getContentSize().height));
// 	_sprite->setTextureRect(Rect(Vec2::ZERO, _sprite->getContentSize()));

	return true;
}

void Sprite1::play(bool isForever)
{
	_isOnce = !isForever;
	scheduleOn();
}

void Sprite1::update(float delta)
{
	static const float s_delta = 0.08f;

	if ((_delta += delta) < (s_delta / _speed))
	{
		return;
	}
	_delta = 0;

	int oldIndex = _frameIndex;

	if ((++_frameIndex) >= getFrameCountPD())
	{
		if (_isOnce)
		{
			scheduleOff();
			_frameIndex = getFrameCountPD() - 1;
			doFrame(oldIndex);
			return;
		}
		_frameIndex = 0;
	}
	setTexture(oldIndex);
	doFrame(oldIndex);
}







