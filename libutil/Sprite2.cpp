#include "Sprite2.h"


bool Sprite2::init(const cpp::sPointer& pointer/*, const WasReader::sColor* colorData*/)
{
	if (!Node::init())
	{
		return false;
	}
	if (!Sprite1::init(pointer/*, colorData*/))
	{
		return false;
	}
	_cascadeColorEnabled = true;
	_cascadeOpacityEnabled = true;

	return true;
}



bool Sprite2::initSprite()
{
	this->addChild(_sprite = Sprite::create());
	const auto& info = *_decoder.info;
	_sprite->setPosition(info.kx, info.height - info.ky);
	this->setContentSize(Size(info.width, info.height));
	this->setAnchorPoint(Vec2(info.kx * 1.0f / info.width, 1 - info.ky * 1.0f / info.height));
	return true;
}



bool Sprite2::setTexture(Texture2D* texture, const sSpFrame& frame)
{
//	NodeGGE::setContentSize(Size(_textures[i]->GetWidth(), _textures[i]->GetHeight()));
	_sprite->setTexture(texture);
	_sprite->setAnchorPoint(Vec2(frame.x / _sprite->getContentSize().width, 1 - frame.y / _sprite->getContentSize().height));
	_sprite->setTextureRect(Rect(Vec2::ZERO, _sprite->getContentSize()));

	return true;
}



bool Sprite2::isBox(const Vec2& worldSpace)
{
	const auto& v = _sprite->convertToNodeSpace(worldSpace);
	if (!Rect(Vec2::ZERO, _sprite->getContentSize()).containsPoint(v))
	{
		return false;
	}
#ifdef ccc_use_engine_g2d
	const auto& tex = _textures[_frameIndex + (int)_direction * getFrameCountPD()];
	auto lock = tex->Lock();
	int x = v.x;
	int y = v.y;
	y = tex->GetHeight() - y;
	bool b = lock[y * tex->GetWidth() + x];
	tex->Unlock();
	return b;
#else 
	return true;
#endif
}
