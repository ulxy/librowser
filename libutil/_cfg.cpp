﻿#include "_cfg.h"

// simsun.ttc
// sans_fallback.ttf
static std::string s_folderRes = ""; // "/storage/emulated/0/Download/lxy/";
static std::string s_fntName = s_folderRes + "simsun.ttc";
static std::string s_folderMap = s_folderRes + "scene/";
static std::string s_folderWdf = s_folderRes + "wdf/";


bool cfg::isFolderRes(std::string path)
{
	path = cpp::replace(path, "\\", "/");
	if (path.empty())
	{
		return false;
	}
	if (path.back() != '/')
	{
		path += '/';
	}
	path += "shader.fx";
	FILE* file = fopen(path.data(), "rb");
	if (file)
	{
		fclose(file);
		return true;
	}
	return false;
}


bool cfg::setFolderRes(const std::string& path)
{
	if (!isFolderRes(path))
	{
		return false;
	}
	s_folderRes = path;
	s_fntName = s_folderRes + "simsun.ttc";
	s_folderMap = s_folderRes + "scene/";
	s_folderWdf = s_folderRes + "wdf/";
	return true;
}


const std::string& cfg::getFolderRes(){ return s_folderRes; }
const std::string& cfg::getFolderMap(){ return s_folderMap; }
const std::string& cfg::getFolderWdf(){ return s_folderWdf; }
const std::string& cfg::getFntName(){ return s_fntName; }


static WdfReader* s_wdfSmap = nullptr;
static WdfReader* s_wdf5 = nullptr;
static WdfReader* s_wdf171 = nullptr;
static WdfReader* s_wdfMagic = nullptr;
static WdfReader* s_wdfSound = nullptr;
static WdfReader* s_wdfMusic = nullptr;
static WdfReader* s_wdfTxt = nullptr;
static WdfReader* s_wdfColor = nullptr;
WdfReader* cfg::getWdfSmap()
{
	if (!s_wdfSmap)
	{
		s_wdfSmap = new WdfReader();
		s_wdfSmap->load(getFolderWdf(), "smap.171");
	}
	return s_wdfSmap;
}

WdfReader* cfg::getWdf5()
{
	if (!s_wdf5)
	{
		s_wdf5 = new WdfReader();
		s_wdf5->load(getFolderWdf(), "sp.5");
		s_wdf5->load(getFolderWdf(), "sp.171");
		s_wdf5->load(getFolderWdf(), "shape.171");
		s_wdf5->load(getFolderWdf(), "magic.171");
	}
	return s_wdf5;
}

WdfReader* cfg::getWdf171()
{
	if (!s_wdf171)
	{
		s_wdf171 = new WdfReader();
		s_wdf171->load(getFolderWdf(), "sp.171");
		s_wdf171->load(getFolderWdf(), "shape.171");
		s_wdf171->load(getFolderWdf(), "magic.171");
		s_wdf171->load(getFolderWdf(), "sp.5");
	}
	return s_wdf171;
}

WdfReader* cfg::getWdfMagic()
{
	if (!s_wdfMagic)
	{
		s_wdfMagic = new WdfReader();
		s_wdfMagic->load(getFolderWdf(), "magic.171");
		s_wdfMagic->load(getFolderWdf(), "sp.171");
		s_wdfMagic->load(getFolderWdf(), "shape.171");
		s_wdfMagic->load(getFolderWdf(), "sp.5");
	}
	return s_wdfMagic;
}

WdfReader* cfg::getWdfSound()
{
	if (!s_wdfSound)
	{
		s_wdfSound = new WdfReader();
		s_wdfSound->load(getFolderWdf(), "sound.5");
		s_wdfSound->load(getFolderWdf(), "sound.171");
	}
	return s_wdfSound;
}

WdfReader* cfg::getWdfMusic()
{
	if (!s_wdfMusic)
	{
		s_wdfMusic = new WdfReader();
		s_wdfMusic->load(getFolderWdf(), "music.171");
	}
	return s_wdfMusic;
}

WdfReader* cfg::getWdfTxt()
{
	if (!s_wdfTxt)
	{
		s_wdfTxt = new WdfReader();
		s_wdfTxt->load(getFolderWdf(), "chat.wdf");
		s_wdfTxt->load(getFolderWdf(), "stock.wdf");
	}
	return s_wdfTxt;
}

WdfReader* cfg::getWdfColor()
{
	if (!s_wdfColor)
	{
		s_wdfColor = new WdfReader();
		s_wdfColor->load(getFolderWdf(), "color.5");
		s_wdfColor->load(getFolderWdf(), "color.171");
	}
	return s_wdfColor;
}




