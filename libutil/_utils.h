#pragma once
// #include "_cfg.h"
#include "EditBox.h"
// #include "NodeMap.h"
#include "NodeModel.h"
#include "ScrollView.h"

#ifdef ccc_use_engine_g2d

#define ccc_main32_begin(_w_, _h_, _title_)\
USING_NS_CC;\
int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)\
{\
	UNREFERENCED_PARAMETER(hPrevInstance);\
	UNREFERENCED_PARAMETER(lpCmdLine);\
	AppGGE app;\
	app.onApplicationDidFinishLaunching = [](Director *director)\
	{\
		auto glview = director->getOpenGLView();\
		Size frameSize((_w_), (_h_));\
		if (!glview)\
		{\
			glview = GLViewImpl::createWithRect((_title_), Rect(Vec2::ZERO, frameSize));\
		}\
		director->setOpenGLView(glview);\
		glview->setDesignResolutionSize(frameSize.width, frameSize.height, ResolutionPolicy::NO_BORDER);




#define ccc_mian32_end(_scene_class_)\
	};\
	app.onRunWithScene = [](Director *director)\
	{\
		director->runWithScene(_scene_class_::create());\
	};\
	app.Start();\
	return 0;\
}
#else
// glview = GLViewImpl::create("lxy");
// frameSize = glview->getFrameSize();
#endif