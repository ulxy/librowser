#pragma once
#include "_utilsinc.h"
#include "WasReader.h"
// #define DIRECTION_UP		 8
// #define DIRECTION_DOWN		 2
// #define DIRECTION_RIGHT		 6
// #define DIRECTION_LEFT		 4
// #define DIRECTION_LEFT_UP	 7
// #define DIRECTION_RIGHT_UP	 9
// #define DIRECTION_LEFT_DOWN  1
// #define DIRECTION_RIGHT_DOWN 3
// enum class eDirection { LD, D, RD, L, R, LU, U, RU };
enum class eDirection { LD, RD, LU, RU, D, L, U, R };
class Sprite1
{
public:
	virtual ~Sprite1();

public:
	virtual bool setDirection(const eDirection direction);
	virtual bool setFrame(int frame);
	virtual void setSpeed(float speed){ _speed = speed; }
	virtual void play(bool isForever);
	virtual int getDirectionsCount()const { return _decoder.info->directionsCount; }
	virtual int getFrameCountPD()const { return  _decoder.info->framesCountPerDirection; }
protected:
	virtual bool init(const cpp::sPointer& pointer/*, const WasReader::sColor* colorData*/);
	virtual bool initSprite(){ return true; }
	virtual bool setTexture(int index);

	virtual bool setTexture(Texture2D* texture, const sSpFrame& frame);
	virtual void update(float delta);
	virtual void scheduleOn(){}
	virtual void scheduleOff(){}
	virtual void doFrame(int frameIndex){}

	eDirection _direction = eDirection::LD;
	int _frameIndex = 0;
//	WasReader _was;
	sSpInfo _info;
	sDecoderEasy _decoder;

	float _speed = 1;
	bool _isOnce = false;
	Texture2D** _textures = nullptr;
private:
	float _delta = 0;
};