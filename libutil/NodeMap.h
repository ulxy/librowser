﻿#pragma once
#include "_utilsinc.h"


class MapReader2 : public MapReader
{
public:
	Astar _astar;

	virtual void initAstar(const bool** fold2s, int w, int h){ _astar.setDataByMap(fold2s, w, h); }
	virtual bool isObstacle(int x, int  y){ return _astar.isObstacle(x, y); }
	virtual vector<sVec2> searchPath(int xStart, int yStart, int xEnd, int yEnd){ return _astar.findAstart(xStart, yStart, xEnd, yEnd); }
};

class NodeMap : public Node
{
public:
	static const int MAP_20 = 20;
	virtual ~NodeMap();
	static NodeMap* create(const string& filename){ CREATE(NodeMap, filename); }

	virtual bool isObstacle(float x, float  y);
	virtual bool isObstacle(const Vec2& v){ return isObstacle(v.x, v.y); }

	virtual bool loadAround(float x, float y);
	virtual bool loadAround(const Vec2& v){ return loadAround(v.x, v.y); }
	virtual bool loadAround();
protected:
	virtual bool init(const string& filename);
	virtual void update(float delta);

	virtual bool loadUnit(int flag);

	MapReader2 _map;
	int _blocksCount;
	int _radiusx;
	int _radiusy;
private:
	int _flag = -3;
	int _x320, _y240;
	bool* _loadFlags = nullptr;
	MapReader::sMask* _masks = nullptr;
	Vec2* _origins = nullptr;
};

