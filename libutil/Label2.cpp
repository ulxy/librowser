﻿#include "Label2.h"
// #include "_cfg.h"

bool Label2::init(const std::string& text, int fontSize)
{
	if (!LabelFont::init("hkyt.ttf"/*cfg::getFntName()*/, fontSize))
	{
		return false;
	}
	if (text.size())
	{
		setString(text);
	}
	return true;
}
