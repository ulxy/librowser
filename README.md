# librowser

galaxy2d引擎+cocos2dx框架 支持节点，导演，调度器 ，事件分发， Action等


# libcocos2d

cocos2dx框架

# libgg

cocos2dx + galaxy2d 浅度结合...

# libgge

galaxy2d游戏引擎(https://www.cnblogs.com/jianguhan/)

# libpng

png图像库(from cocos2dx)

# libutil

自己封装的一些零件

# 示意图

![demo](https://gitee.com/ulxy/librowser/raw/master/diagram/demo.png)

![save](https://gitee.com/ulxy/librowser/raw/master/diagram/save.png)

# 开发环境

Visual Studio Community 2017

![installer](https://gitee.com/ulxy/diagram_images/raw/master/installer.png)