#pragma once
#include "_inc.h"


class TestInput : public Scene
{
public:
	CREATE_FUNC(TestInput);
protected:

	virtual bool init()
	{
		Scene::init();

		Label2 *label = Label2::create();
		this->addChild(label, 0, 1);
		label->setPositionY(cc::vsCenter().y + 100);

		Label2 *label2 = Label2::create();
		this->addChild(label2, 0, 2);
		label2->setPosition(cc::vsCenter());

		ListenerSnatch* listenSnatch = ListenerSnatch::create(this);

		for (int i = 0; i < 5; ++i)
		{
			EditBox* eb = EditBox::create(toString("������%d", i), 6 + i * 2);
			eb->setPosition(cc::vsRand());
			listenSnatch->addChildSnatch(eb);
		}

		scheduleUpdate();
		return true;
	}
	





	virtual void update(float delta)
	{
		auto c = gge::Input_GetChar();
		if (c[0])
		{
		//	bool b = cpp::isAscii(c);
			int l = strlen(c);
		//	asert((b && l == 1) || (!b && l == 2), "");
			Label2 *label = (Label2*)getChildByTag(1);
			label->setString(label->getString() + c);

			Label2 *label2 = (Label2*)getChildByTag(2);
			l = 0;
			string s = label->getString();
			forv(s, i)
			{
				if (s[i] & 0x80)
				{
					++l;
				}
			}
			label2->setString(toString("%d",s.length() - l / 2));
		}
	}
};