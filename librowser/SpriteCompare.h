#pragma once
#include "PadWas.h"
#include "WdfReader.h"


struct sSp { ushort dCount, fCountPD, m_w, m_h, kx, ky; };
struct sSpCompare
{
	bool operator()(const sSp& s2, const sSp& s)
	{
		if (s2.m_w != s.m_w)return s2.m_w < s.m_w;
		if (s2.m_h != s.m_h)return s2.m_h < s.m_h;
		if (s2.kx != s.kx)return s2.kx < s.kx;
		if (s2.ky != s.ky)return s2.ky < s.ky;
		if (s2.dCount != s.dCount)return s2.dCount < s.dCount;
		if (s2.fCountPD != s.fCountPD)return s2.fCountPD < s.fCountPD;
		return false;
	}
};
inline bool isSp(const char* ptr) { return (*(ushort*)ptr) == 0x5053; }
inline void getSp(sSp& sp, const char* ptr)
{
	sp.dCount = *(((ushort*)ptr) + 2);
	sp.fCountPD = *(((ushort*)ptr) + 3);
	sp.m_w = *(((ushort*)ptr) + 4);
	sp.m_h = *(((ushort*)ptr) + 5);
	sp.kx = *(((ushort*)ptr) + 6);
	sp.ky = *(((ushort*)ptr) + 7);
}


class SpriteCompare : public PadWas
{
public:
	static SpriteCompare* create(const string& name, const string& name2 = ""){ CREATE(SpriteCompare, name, name2); }

	virtual bool init(const string& name, const string& name2)
	{
		if (!PadWas::init())
		{
			return false;
		}
#if 0
		WdfReader r5;
		r5.loads("C:/Users/deen/Desktop/日期版/wdf/");
		const auto& its = cct::getWeapons();
		const auto& rs = cct::getRoles();
		vector<ulong> uws, urs;
		forv(its, i)
		{
			if (its[i].e != cct::eItem::斧钺f)
			{
				continue;
			}
			const auto& as = its[i].actions;
			int y = 0;
			int ix = 190;
			int iy = 150;
			forv(as, k)
			{
				if (k < 5)
				{
//					continue;
				}
				const auto& wa = as[k].action;
				const auto& ra = rs[(int)as[k].role].actions[as[k].actionsIndex].action;
				uws = { wa.stand, wa.walk, wa.await, wa.go, wa.atk, wa.maigc, wa.def, wa.atkd, wa.dead, wa.back };
				urs = { ra.stand, ra.walk, ra.await, ra.go, ra.atk, ra.maigc, ra.def, ra.atkd, ra.dead, ra.back };

				forv(uws, j)
				{
					SpriteBtn* spr = SpriteBtn::create(r5.getPointer(urs[j]));
					initRank(spr, ccc_u2s(uws[j]));

					spr->getParent()->setPosition(j * ix + 40, y * iy + 20);
					if (uws[j])
					{
						Sprite2* spr2 = Sprite2::create(r5.getPointer(uws[j]));
						spr2->setPosition(spr->getPosition());
						spr->getParent()->addChild(spr2);
						spr2->play(true);
					}
				}
				++y;
//				continue;

				const auto& wa2 = as[k].action2;
				const auto& ra2 = rs[(int)as[k].role2].actions[as[k].actionsIndex2].action;
				uws = { wa2.stand, wa2.walk, wa2.await, wa2.go, wa2.atk, wa2.maigc, wa2.def, wa2.atkd, wa2.dead, wa2.back };
				urs = { ra2.stand, ra2.walk, ra2.await, ra2.go, ra2.atk, ra2.maigc, ra2.def, ra2.atkd, ra2.dead, ra2.back };

				forv(uws, j)
				{
					SpriteBtn* spr = SpriteBtn::create(r5.getPointer(urs[j]));
					initRank(spr, ccc_u2s(uws[j]));

					spr->getParent()->setPosition(j * ix + 40, y * iy + 20);
					Sprite2* spr2 = Sprite2::create(r5.getPointer(uws[j]));
					spr2->setPosition(spr->getPosition());
					spr->getParent()->addChild(spr2);
					spr2->play(true);
				}
				++y;
			}
		}

		initScroll();
		return true;
#endif
#if 0
		WdfReader r5;
		r5.loads("C:/Users/deen/Desktop/日期版/wdf/");
		auto its = cct::getPets();
		forv(its, i)
		{
			if (its[i].e != eModel::增长巡守zz)
			{
				continue;
			}
// 			if (its[i].small2 != its[i].big2)
// 			{
// 				continue;
// 			}
//			for (const auto& as : its[i].actions)
			{
				const auto& a = its[i].actions[1].action;
//				const auto& a = as.action;
				vector<ulong> uids = { a.stand, a.walk, a.await, a.go, a.atk, a.atk2, a.maigc, a.def, a.atkd, a.dead, a.back };
				forv(uids, k)
				{
					if (!uids[k])
					{
						continue;
					}
					SpriteBtn* spr = SpriteBtn::create(r5.getPointer(uids[k]));
					if (spr &&
						(k == 0 && spr->getFrameCountPD() == 19 || 1) && // 站立
						(k == 1 && spr->getFrameCountPD() == 8 || 1) && // 走
						(k == 2 && spr->getFrameCountPD() == 8 || 1) && // 战斗
						(k == 3 && spr->getFrameCountPD() == 4 || 1) && // 前进
						(k == 4 && spr->getFrameCountPD() == 15 || 1) && // 攻击
						(k == 5 && spr->getFrameCountPD() == 8 || 1) && // 攻击2
						(k == 6 && spr->getFrameCountPD() == 19 || 1) && // 施法
						(k == 7 && spr->getFrameCountPD() == 2 || 1) && // 防御
						(k == 8 && spr->getFrameCountPD() == 2 || 1) && // 被打
						(k == 9 && spr->getFrameCountPD() == 10 || 1) &&  //死亡
						(k == 10 && spr->getFrameCountPD() == 7 || 1)  //返回亡
						)
					{
						initRank(spr, its[i].name/*ccc_u2s(its[i].small)*/);
					}
				}
			}

			continue;
			SpriteBtn* spr = SpriteBtn::create(r5.getPointer(0/*a.stand*/));
			if (spr)
			{
				initRank(spr, its[i].name/*ccc_u2s(its[i].small)*/);
			}
			continue;
			spr = SpriteBtn::create(r5.getPointer(its[i].big2));
			if (spr)
			{
				initRank(spr, ccc_u2s(its[i].big));
			}
			continue;
// 			spr = SpriteBtn::create(r5.getPointer(its[i].dialog));
// 			if (spr)
// 			{
// 				initRank(spr, ccc_u2s(its[i].dialog));
// 			}

			spr = SpriteBtn::create(r5.getPointer(its[i].small2));
			if (spr)
			{
				initRank(spr, ccc_u2s(its[i].small2));
			}
			spr = SpriteBtn::create(r5.getPointer(its[i].big2));
			if (spr)
			{
				initRank(spr, ccc_u2s(its[i].big2));
			}
// 			spr = SpriteBtn::create(r5.getPointer(its[i].dialog2));
// 			if (spr)
// 			{
// 				initRank(spr, ccc_u2s(its[i].dialog2));
// 			}
		}

		initScroll();
		return true;
#endif
		WdfReader r, r2;
		r.load(name);
		string str2;
		if (!name2.empty())
		{
			r2.load(name2);
			str2 = r2.getWdfs().front().filename;
		}
		const auto& wdf5 = r.getWdfs().front();


		static sSp sp;
		static char ptr18[18];

//		int i = -1;
		for (int i = 0; i < wdf5.indexsCount; ++i)
//		for (auto m : s_uuMap)
		{
//			++i;
			const auto& index5 = wdf5.indexs[i];
			FILE* file = fopen(name.c_str(), "rb");
			fseek(file, index5.offset, SEEK_SET);
			fread(ptr18, 1, 18, file);
			if (!isSp(ptr18))
			{
				fclose(file);
				continue;
			}
			getSp(sp, ptr18);
			fclose(file);
			if (sp.dCount == 1 && sp.fCountPD == 1)
			{
//				continue;
			}
			if (sp.m_w > 320)
			{
//				continue;
			}

			SpriteBtn* spr = SpriteBtn::create(r.getPointer(&index5, wdf5.filename));
			if (!spr)
			{
				continue;
			}

			Sprite2* spr2 = nullptr;
			if (!name2.empty())
			{
				spr2 = Sprite2::create(r2.getPointer(&r2.getWdfs().front().indexs[i], str2));
			}
			initRank(spr, ccc_u2s(index5.uid), spr2);
		}

		initScroll();
		return true;
	}

};


