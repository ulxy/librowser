﻿#pragma once
#include "../libcc/_cpp.h"

class WdfReader
{
public:
	WdfReader();
	~WdfReader();

	struct sIndex
	{
		ulong uid;
		ulong offset;
		ulong size;
		ulong space;
	};

	struct sWdf
	{
		sIndex* indexs = nullptr;
		int indexsCount;
		string filename;
	};

	bool clear();
	bool load(const string &filename);
	bool load(const string &path, const string &filename);
	bool loads(const string &path);

	cpp::sPointer getPointer(const sIndex* index, const string& filename);
	cpp::sPointer getPointer(const ulong& uid, const string& filename = "");

	const sIndex* getIndex(const ulong& uid, string& filename)const;
	const sIndex* getIndex(const ulong& uid)const;

	string getPath()const{ return _path; }

	const vector<sWdf>& getWdfs()const{ return _wdfs; }

	void clearWdfs(){ _wdfs.clear(); }
private:
	string _path;
	vector<sWdf> _wdfs;
// 	ulong s_allocSize;
// 	char *s_ptr = nullptr;
};